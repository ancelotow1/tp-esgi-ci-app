//
//  CocktailApiService.swift
//  tp-esgi-ci-app
//
//  Created by Owen Ancelot on 30/01/2023.
//

import Foundation

class CocktailApiService: ICocktailService {
    
    private let uriApi = "https://crudcrud.com/api/3356072ea1ab41bca68f54cf46793d43/tp-esgi-ci-app"
    
    func getAll(_ completion: @escaping (Error?, [Cocktail]?) -> Void) {
        let urlComponents = URLComponents(string: uriApi)
        var urlRequest = URLRequest(url: urlComponents!.url!)
        urlRequest.httpMethod = "GET"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = URLSession.shared.dataTask(with: urlRequest) { data, response, err in
            guard err == nil else {
                completion(err, nil)
                return
            }
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(NSError(domain: "com.esgi", code: 2, userInfo: [
                    NSLocalizedFailureReasonErrorKey: "Error HTTP"
                ]), nil)
                return
            }
            if httpResponse.statusCode == 400 {
                completion(NSError(domain: "com.esgi", code: 2, userInfo: [
                    NSLocalizedFailureReasonErrorKey: "Bad request"
                ]), nil)
                return
            }
            
            guard let d = data else {
               completion(NSError(domain: "com.esgi", code: 2, userInfo: [
                   NSLocalizedFailureReasonErrorKey: "No data found"
               ]), nil)
               return
           }
           do {
               let json = try JSONSerialization.jsonObject(with: d, options: .allowFragments)
               guard let dict = json as? [[String: Any]] else {
                   completion(NSError(domain: "com.esgi", code: 3, userInfo: [
                       NSLocalizedFailureReasonErrorKey: "Invalid format"
                   ]), nil)
                   return
               }
               var cocktails: [Cocktail] = []
               for value in dict {
                   var cocktail = Cocktail.fromDictionary(json: value)
                   if let cocktail = cocktail {
                       cocktails.append(cocktail)
                   }
               }
               completion(nil, cocktails)
           } catch {
               completion(err, nil)
               return
           }
        }
        task.resume()
        
        completion(nil, [])
    }
    
    func deleteById(id: String, _ completion: @escaping (Error?) -> Void) {
        completion(nil)
    }
    
    func add(_ cocktail: Cocktail, _ completion: @escaping (Error?) -> Void) {
        let body = cocktail.toDictionary()
        let urlComponents = URLComponents(string: uriApi)
        var urlRequest = URLRequest(url: urlComponents!.url!)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = try? JSONSerialization.data(withJSONObject: body)
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = URLSession.shared.dataTask(with: urlRequest) { data, response, err in
            guard err == nil else {
                completion(err)
                return
            }
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(NSError(domain: "com.esgi", code: 2, userInfo: [
                    NSLocalizedFailureReasonErrorKey: "Error HTTP"
                ]))
                return
            }
            if httpResponse.statusCode < 400 {
                completion(nil)
                return
            }
            completion(NSError(domain: "com.esgi", code: 2, userInfo: [
                NSLocalizedFailureReasonErrorKey: "No added"
            ]))
            return
        }
        task.resume()
    }
    
}
