//
//  Cocktail.swift
//  tp-esgi-ci-app
//
//  Created by Owen Ancelot on 30/01/2023.
//

import Foundation

class Cocktail {
    
    let id: String
    let nom: String
    let price: Double
    let alcool: String
    let ingredients: String
    
    init(id: String, nom: String, price: Double, alcool: String, ingredients: String) {
        self.id = id
        self.nom = nom
        self.price = price
        self.alcool = alcool
        self.ingredients = ingredients
    }
    
    func toDictionary() -> [String: Any] {
        var json = [String: Any]()
        json["nom"] = self.nom
        json["price"] = self.price
        json["alcool"] = self.alcool
        json["ingredients"] = self.ingredients
        return json
    }
    
    static func fromDictionary(json: [String: Any]) -> Cocktail? {
        guard let nom = json["nom"] as? String,
              let price = json["price"] as? Double,
              let alcool = json["alcool"] as? String,
              let ingredients = json["ingredients"] as? String else {
            return nil
        }
        let id = json["_id"] as? String ?? ""
        return Cocktail(id: id, nom: nom, price: price, alcool: alcool, ingredients: ingredients)
    }
}
