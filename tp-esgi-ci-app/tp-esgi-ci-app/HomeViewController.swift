//
//  HomeViewController.swift
//  tp-esgi-ci-app
//
//  Created by Owen Ancelot on 30/01/2023.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let service: ICocktailService = CocktailInMemoryService()
    var cocktails: [Cocktail] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.navigationItem.rightBarButtonItems = [
            self.getAddButtonItem()
        ]
    }
    
    fileprivate func getAddButtonItem() -> UIBarButtonItem {
        return UIBarButtonItem(systemItem: .add, primaryAction: UIAction(handler: { _ in
            let alert = UIAlertController(title: "Création de note", message: nil, preferredStyle: .alert)
            alert.addTextField { textField in
                textField.placeholder = "Nom"
            }
            alert.addTextField { textField in
                textField.placeholder = "Prix"
                textField.keyboardType = UIKeyboardType.decimalPad
            }
            alert.addTextField { textField in
                textField.placeholder = "Alcool"
            }
            alert.addTextField { textField in
                textField.placeholder = "Ingrédients"
            }
            alert.addAction(UIAlertAction(title: "Valider", style: .default) { _ in
                let nom = alert.textFields![0].text ?? ""
                let prix = Double(alert.textFields![1].text ?? "0.0") ?? 0.00
                let alcool = alert.textFields![2].text ?? ""
                let ingredients = alert.textFields![3].text ?? ""
                
                let cocktail = Cocktail(id: "AA", nom: nom, price: prix, alcool: alcool, ingredients: ingredients)
                
                self.service.add(cocktail) { error in
                    guard error == nil else {
                        return
                    }
                    self.realoadCocktails()
                }
            })
            alert.addAction(UIAlertAction(title: "Annuler", style: .cancel))
            self.present(alert, animated: true)
        }))
    }
    
    func realoadCocktails() {
        self.service.getAll() { error, cocktails in
            guard error == nil else {
                return
            }
            self.cocktails = cocktails!
            self.tableView.reloadData()
        }

    }

}

extension HomeViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cocktails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        let cocktail = self.cocktails[indexPath.item]
        cell.textLabel?.text = "\(cocktail.nom) (\(cocktail.price)€)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
      if editingStyle == .delete {
          let cocktail = self.cocktails[indexPath.item]
          self.service.deleteById(id: cocktail.id) { error in
              guard error == nil else {
                  return
              }
              self.realoadCocktails()
          }
      }
    }
    
}
