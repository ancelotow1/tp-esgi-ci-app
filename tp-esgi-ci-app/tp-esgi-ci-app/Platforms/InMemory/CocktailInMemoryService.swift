//
//  CocktailInMemoryService.swift
//  tp-esgi-ci-app
//
//  Created by Owen Ancelot on 30/01/2023.
//

import Foundation

class CocktailInMemoryService: ICocktailService {
    
    private var cocktails: [Cocktail] = []
    
    func add(_ cocktail: Cocktail, _ completion: @escaping (Error?) -> Void) {
        cocktails.append(cocktail)
        completion(nil)
    }
    
    func deleteById(id: String, _ completion: @escaping (Error?) -> Void) {
        for i in 0...cocktails.count {
            if(cocktails[i].id == id) {
                cocktails.remove(at: i)
                break
            }
        }
        completion(nil)
    }
    
    func getAll(_ completion: @escaping (Error?, [Cocktail]?) -> Void) {
        completion(nil, cocktails)
    }
    
}
