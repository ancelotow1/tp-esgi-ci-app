//
//  ICocktailService.swift
//  tp-esgi-ci-app
//
//  Created by Owen Ancelot on 30/01/2023.
//

import Foundation

protocol ICocktailService {
    
    func getAll(_ completion: @escaping (Error?, [Cocktail]?) -> Void) -> Void
    
    func deleteById(id: String, _ completion: @escaping (Error?) -> Void) -> Void
    
    func add(_ cocktail: Cocktail, _ completion: @escaping (Error?) -> Void) -> Void
    
}
